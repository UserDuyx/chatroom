<?php

function pre($data = '', $exit = TRUE)
{
    echo '<pre>';
    print_r($data);
    if ($exit)
        die;
}

function public_url($url = '')
{
    return base_url('public/' . $url);
}
