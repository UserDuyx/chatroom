<div class="row">
    <div class="col-lg-3 col-md-4 col-md-5 div-in-row-1">
        <input type="hidden" name="login_user_id" id="login_user_id" value="<?= $userdata[$url]->uid ?>">
        <input type="hidden" name="is_active_chat" id="is_active_chat" value="0">
        <div class="mt-3 mb-3 text-center">
            <img src="<?= public_url('img/') . $userdata[$url]->user_profile ?>" width="150" height="150" class="img-fluid rounded-circle img-thumbnail avatar" alt="Ảnh đại diện">
            <h3 class="mt-2"><?= $userdata[$url]->user_name ?></h3>
            <a href="<?= site_url('trang-chu/' . $userdata[$url]->uid) ?>" class="btn btn-danger mt-2 mb-2">Phòng Chat</a>
            <a href="<?= site_url('ca-nhan/' . $userdata[$url]->uid) ?>" class="btn btn-secondary mt-2 mb-2">Chỉnh Sửa</a>
            <a href="<?= site_url('dang-xuat/' . $userdata[$url]->uid) ?>" class="btn btn-primary mt-2 mb-2">Đăng Xuất</a>
            <button type="submit" onclick="window.location.reload()" class="btn btn-success">Cập nhật</button>
        </div>
        <div class="list-group div-in-row-2">
            <?php foreach ($private_user as $key => $user) : ?>
                <?php $icon = '<i class="fa fa-circle text-danger"></i>' ?>
                <?php $unread = '' ?>
                <?php if ($user->user_login_status == 1) : ?>
                    <?php $icon = '<i class="fa fa-circle text-success"></i>' ?>
                <?php endif ?>
                <?php if ($userdata[$url]->uid != $user->uid) : ?>
                    <?php if ($user->count_status > 0) : ?>
                        <?php $unread = '<span class="badge badge-danger badge-pill">' . $user->count_status . '</span>' ?>
                    <?php endif ?>
                    <a class="list-group-item list-group-item-action select_user" data-userid="<?= $user->uid ?>">
                        <img src="<?= public_url('img/') . $user->user_profile ?>" width="50" height="50" class="img-fluid rounded-circle img-thumbnail avatar" alt="Ảnh đại diện">
                        <span class="ml-1">
                            <strong>
                                <span id="list_user_name_<?= $user->uid ?>"><?= $user->user_name ?></span>
                                <span id="user_id_<?= $user->uid ?>"><?= $unread ?></span>
                                <span class="mt-2 float-right" id="userstatus_<?= $user->uid ?>"><?= $icon ?></span>
                            </strong>
                        </span>
                    </a>
                <?php endif ?>


            <?php endforeach ?>
        </div>
    </div>

    <div class="col-lg-9 col-md-8 col-sm-7">
        <h3 class="text-center">Tin nhắn riêng tư</h3>
        <hr>
        <div id="chat_area">

        </div>
    </div>
</div>

