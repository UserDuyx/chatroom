<h1 class="text-center"><b>Phòng Chat</b></h1>
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col col-sm-6">
                        <h3>Tin nhắn</h3>
                    </div>
                    <div class="col col-sm-6 text-right">
                        <a href="<?= site_url('nhan-rieng/' . $url) ?>" class="btn btn-success btn-sm">Nhắn riêng</a>
                    </div>
                </div>
            </div>
            <div class="card-body" id="messages_area">
                <?php foreach ($messages as $msg) : ?>
                    <?php if ($msg->user_uid == $url) : ?>
                        <div class='row justify-content-end'>
                            <div class='col-sm-10'>
                                <div class='shadow-sm alert alert-success text-dark '><b>Bạn: </b><?= $msg->msg ?></br>
                                    <div class='text-right'><small><i><?= $msg->time ?></i></small></div>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class='row justify-content-start'>
                            <div class='col-sm-10'>
                                <div class='shadow-sm alert alert-light text-dark '><b><?= $msg->user_name ?>: </b><?= $msg->msg ?></br>
                                    <div class='text-right'><small><i><?= $msg->time ?></i></small></div>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </div>
        </div>
        <form action="" method="post" id="chat-form" data-parsley-errors-container="#validation_error">
            <div class="input-group mb-3">
                <textarea class="form-control" name="chat-message" id="chat_message" placeholder="Nhập tin nhắn..." maxlength="2000" required data-parsley-required-message="Tin nhắn mà không có tin nhắn thì sao gọi là tin nhắn được?"></textarea>
                <div class="input-group-append">
                    <button type="submit" class="btn btn-primary" name="send" id="send"><i class="fa fa-paper-plane"></i></button>
                </div>
            </div>
            <div id="validation_error"></div>
        </form>
    </div>
    <div class="col-lg-4">
        <input type="hidden" name="login-user-id" id="login-user-id" value="<?= $userdata[$url]->uid ?>">
        <div class="mt-3 mb-3 text-center">
            <img src="<?= public_url('img/') . $userdata[$url]->user_profile ?>" width="150" height="150" class="img-fluid rounded-circle img-thumbnail avatar" alt="Ảnh đại diện">
            <h3 class="mt-2"><?= $userdata[$url]->user_name ?></h3>
            <a href="<?= site_url('ca-nhan/' . $userdata[$url]->uid) ?>" class="btn btn-secondary mt-2 mb-2">Chỉnh Sửa</a>
            <a href="<?= site_url('dang-xuat/' . $userdata[$url]->uid) ?>" class="btn btn-primary mt-2 mb-2">Đăng Xuất</a>
        </div>
        <div class="card mt-3">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">Người dùng</div>
                    <div class="col-md-6 text-right">
                        <button type="submit" onclick="window.location.reload()" class="btn btn-primary">Cập nhật</button>
                    </div>
                </div>
            </div>
            <div class="card-body" id="user_list">
                <div class="list-group list-group-flush">
                    <?php foreach ($online as $user) : ?>
                        <?php if ($user->user_login_status == 1) : ?>
                            <a class="list-group-item list-group-item-action">
                                <img src="<?= public_url('img/') . $user->user_profile ?>" width="50" height="50" class="img-fluid rounded-circle img-thumbnail online-avatar" alt="Ảnh đại diện">
                                <span class="ml-1"><strong><?= $user->user_name ?></strong> <?= $userdata[$url]->uid == $user->uid ? '(bạn)' : '' ?></span>
                                <span class="mt-2 float-right"><i class="fa fa-circle text-success"></i></span>
                            </a>
                        <?php else : ?>
                            <a class="list-group-item list-group-item-action">
                                <img src="<?= public_url('img/') . $user->user_profile ?>" width="50" height="50" class="img-fluid rounded-circle img-thumbnail online-avatar" alt="Ảnh đại diện">
                                <span class="ml-1"><strong><?= $user->user_name ?></strong></span>
                                <span class="mt-2 float-right"><i class="fa fa-circle text-danger"></i></span>
                            </a>
                        <?php endif  ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>

