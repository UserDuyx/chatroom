<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">


    <link href="<?= public_url('vendor-front') ?>/bootstrap/bootstrap.min.css" rel="stylesheet">

    <link href="<?= public_url('vendor-front') ?>/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" type="text/css" href="<?= public_url('vendor-front') ?>/parsley/parsley.css" />

    <script src="<?= public_url('vendor-front') ?>/jquery/jquery.min.js"></script>
    <script src="<?= public_url('vendor-front') ?>/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="<?= public_url('vendor-front') ?>/jquery-easing/jquery.easing.min.js"></script>

    <script type="text/javascript" src="<?= public_url('vendor-front') ?>/parsley/dist/parsley.min.js"></script>
    <script src="<?= public_url('vendor-front') ?>/parsley/dist/i18n/vi.js"></script>
    <?php $controller = $this->uri->segment(1) ?>
    <style>
        div.container {
            font-family: 'Noto Sans', sans-serif !important;
            font-family: 'Open Sans', sans-serif !important;
            font-family: 'Roboto', sans-serif !important;
            margin-top: 75px;
        }

        .parsley-required {
            margin-top: 12px;
        }

        .parsley-type {
            margin-top: 12px;
        }

        .parsley-minlength {
            margin-top: 12px;
        }

        html,
        body {
            height: 100%;
            width: 100%;
        }

        #wrapper {
            display: flex;
            flex-flow: column;
            height: 100%;
        }

        #remaining {
            flex-grow: 1;
        }

        #messages {
            height: 200px;
            background: whitesmoke;
            overflow: auto;
        }

        #chat-room-frm {
            margin-top: 10px;
        }

        #user_list {
            height: 450px;
            overflow-y: auto;
        }

        <?php if ($controller == 'trang-chu') : ?>#messages_area {
            height: 650px;
            overflow-y: auto;
            background-color: #e6e6e6;
        }

        .avatar {
            height: 150px !important;
        }

        .online-avatar {
            height: 50px !important;
        }

        <?php else : ?>#messages_area {
            height: 75vh;
            overflow-y: auto;
        }

        .div-in-row-1 {
            background-color: #f1f1f1;
            height: 100vh;
            border-right: 1px solid #ccc;
        }

        .div-in-row-2 {

            max-height: 100vh;
            overflow-y: scroll;
            margin-bottom: 10px;
            -webkit-overflow-scrolling: touch;

        }

        .select_user {
            cursor: pointer;
        }

        <?php endif ?>
    </style>
</head>

<body>

    <?php $array = ['trang-chu', 'ca-nhan', 'dang-nhap', 'dang-ky'];  ?>
    <div class="container<?= in_array($controller, $array) ? '' : '-fluid' ?>">
        <?php $this->load->view($temp, $this->data) ?>
        <?php $this->load->view('room/static/footer') ?>
    </div>
    <?php if ($controller != 'nhan-rieng') : ?>
        <script type="text/javascript">
            $(document).ready(function() {
                var conn = new WebSocket('ws://127.0.0.1:8080');

                conn.onopen = function(e) {
                    //
                };

                conn.onmessage = function(e) {

                    var data = JSON.parse(e.data);
                    var html_data = '';
                    if (data.from == "Bạn") {
                        html_data = "<div class='row justify-content-end'><div class='col-sm-10'><div class='shadow-sm alert alert-success text-dark '><b>" + data.from + ": </b>" + data.msg + "</br><div class='text-right'><small><i>" + data.dt + "</i></small></div></div></div></div>";
                    } else {
                        html_data = "<div class='row justify-content-start'><div class='col-sm-10'><div class='shadow-sm alert alert-light text-dark '><b>" + data.from + ": </b>" + data.msg + "</br><div class='text-right'><small><i>" + data.dt + "</i></small></div></div></div></div>";
                    }

                    $('#messages_area').append(html_data);
                    $('#chat_message').val(" ");

                };

                $('#chat-form').parsley();

                <?php if ($controller == 'trang-chu') : ?>
                    $('#messages_area').scrollTop($('#messages_area')[0].scrollHeight);
                <?php endif ?>


                $('#chat-form').on('submit', function(event) {
                    event.preventDefault();

                    var user_id = $('#login-user-id').val();
                    var message = $('#chat_message').val();
                    var data = {
                        userId: user_id,
                        msg: message
                    };
                    conn.send(JSON.stringify(data));

                });

            });
        </script>
    <?php else : ?>
        <script type="text/javascript">
            $(document).ready(function() {

                var receiver_user_id = '';
                var conn = new WebSocket('ws://127.0.0.1:8080?token=<?= $userdata[$this->url]->token ?>');

                conn.onopen = function(e) {
                    //
                };

                conn.onmessage = function(e) {

                    var data = JSON.parse(e.data);
                    var html_data = '';
                    if (receiver_user_id == data.userId || data.from == 'Bạn') {
                        if ($('#is_active_chat').val() == 1) {
                            if (data.from == "Bạn") {
                                html_data = "<div class='row justify-content-end'><div class='col-sm-10'><div class='shadow-sm alert alert-success text-dark '><b>" + data.from + ": </b>" + data.msg + "</br><div class='text-right'><small><i>" + data.dt + ".</i></small></div></div></div></div>";
                            } else {
                                html_data = "<div class='row justify-content-start'><div class='col-sm-10'><div class='shadow-sm alert alert-light text-dark '><b>" + data.from + ": </b>" + data.msg + "</br><div class='text-right'><small><i>" + data.dt + ".</i></small></div></div></div></div>";
                            }
                            $('#messages_area').append(html_data);
                            $('#messages_area').scrollTop($('#messages_area')[0].scrollHeight);
                            $('#chat_message').val(" ");
                        }
                    } else {
                        console.log("here");
                        var count_chat = $('#userid' + data.userId).text();

                        if (count_chat == '') {
                            count_chat = 0;
                        }
                        count_chat++;
                        $('#user_id_' + data.userId).html('<span class="badge badge-danger badge-pill">' + count_chat + '</span>');
                    }

                };

                $('#chat-form').parsley();

                function make_chat_area(user_name) {
                    var html = `
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col col-sm-6">
                                        <b>Đang trò chuyện riêng với: <span class="text-danger" id="chat_user_name">` + user_name + `</span></b>
                                    </div>
                                    <div class="col col-sm-6 text-right">
                                        <button type="button" class="close" id="close_chat_area" data-dismiss="alert" aria-label="Close">
                                            <span aria-hiden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body" id="messages_area">

                            </div>
                        </div>

                        <form id="chat_form" method="POST" data-parsley-errors-container="#validation_error">
                            <div class="input-group mb-3">
                                <textarea class="form-control" name="chat_message" id="chat_message" placeholder="Nhập tin nhắn..." maxlength="2000" required data-parsley-required-message="Tin nhắn mà không có tin nhắn thì sao gọi là tin nhắn được?"></textarea>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary" name="send" id="send"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                            <div id="validation_error"></div>
                        </form>
                    `;

                    $('#chat_area').html(html);
                    $('#chat_form').parsley();
                }

                $(document).on('click', '.select_user', function() {
                    receiver_user_id = $(this).data('userid');

                    var from_user_id = $('#login_user_id').val();

                    var receiver_user_name = $('#list_user_name_' + receiver_user_id).text();

                    $('.select_user.active').removeClass('active');
                    $(this).addClass('active');
                    make_chat_area(receiver_user_name);

                    $('#is_active_chat').val(1);

                    $.ajax({
                        url: "<?= site_url('pchat/handle') ?>",
                        method: "POST",

                        data: {
                            action: 'fetch_chat',
                            to: receiver_user_id,
                            from: from_user_id
                        },

                        success: function(response) {

                            var html_data = response;

                            $('#user_id_' + receiver_user_id).html('');
                            $('#messages_area').html(html_data);
                            $('#messages_area').scrollTop($('#messages_area')[0].scrollHeight);

                        }
                    });

                    $('#chat_form').on('submit', function(event) {
                        event.preventDefault();

                        var user_id = $('#login_user_id').val();
                        var message = $('#chat_message').val();
                        var data = {
                            userId: user_id,
                            msg: message,
                            to: receiver_user_id,
                            command: 'private'
                        };
                        conn.send(JSON.stringify(data));
                    });
                });

                $(document).on('click', '#close_chat_area', function() {
                    $('#chat_area').html('');

                    $('.select_user.active').removeClass('active');

                });



            });
        </script>
    <?php endif ?>
    <script>
        $(document).ready(function() {
            $('#auth_form').parsley();
        });
    </script>
</body>

</html>