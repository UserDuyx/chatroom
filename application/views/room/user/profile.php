
<h1 class="text-center"><b>Quản Lý Tài Khoản</b></h1>
<?php if (!empty($this->session->flashdata('notice'))) : ?>
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <p><?= $this->session->flashdata('notice') ?></p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Đóng">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif ?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-6">Thông Tin Tài Khoản</div>
            <div class="col-md-4 text-right"><a href="<?= site_url('nhan-rieng/' . $userdata[$url]->uid) ?>" class="btn btn-success btn-sm">Quay về chat riêng</a></div>
            <div class="col-md-2 text-right"><a href="<?= site_url('trang-chu/' . $userdata[$url]->uid) ?>" class="btn btn-warning btn-sm">Quay về phòng chat</a></div>
        </div>
    </div>
    <div class="card-body">
        <form id="auth_form" action="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Tên Tài Khoản:</label>
                <input type="text" name="username" id="username" class="form-control" value="<?= $userdata[$url]->user_name ?>" required data-parsley-required-message="Tên tài khoản không được bỏ trống">
            </div>
            <div class="form-group">
                <label>Email:</label>
                <input type="email" name="email" id="email" class="form-control" value="<?= $userdata[$url]->user_email ?>" required disabled>
            </div>
            <div class="form-group">
                <label>Đổi Mật Khẩu:</label>
                <input type="password" name="password" id="password" class="form-control" data-parsley-required-message="Mật khẩu không được bỏ trống" minlength="4" data-parsley-minlength-message="Mật khẩu phải có ít nhất 4 ký tự">
            </div>
            <div class="form-group">
                <label>Ảnh Hồ Sơ</label>

                <input type="file" name="profile" class="form-control">
                <p><small>Ảnh nên là hình vuông</small></p>

                <img src="<?= public_url('img/') . $userdata[$url]->user_profile ?>" alt="Ảnh hồ sơ" width="100" class="img-fluid img-thumbnail mt-3">
                <input type="hidden" value="<?= $userdata[$url]->user_profile ?>">
            </div>
            <br>
            <div class="form-group text-center">
                <input type="submit" class="btn btn-primary" value="Sửa">
            </div>
        </form>
    </div>
</div>