<h1 class="text-center"><b>Chào mừng bạn đến với phòng chat</b></h1>
<div class="row justify-content-md-center">
    <div class="col col-md-4 mt-5">
        <?php if (!empty(form_error('submit'))) : ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <p><?= form_error('submit') ?></p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Đóng">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif ?>
        <?php if (!empty($this->session->flashdata('notice'))) : ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <p><?= $this->session->flashdata('notice') ?></p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Đóng">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif ?>
        <?php if (!empty($message)) : ?>
            <div class="alert alert-success">
                <?= $message ?>
            </div>
        <?php endif ?>
        <div class="card">
            <div class="card-header">
                <b>Đăng Nhập Vào Phòng Chat</b>
            </div>
            <div class="card-body">
                <form id="auth_form" action="" method="POST">
                    <div class="form-group mt-3">
                        <label for="email">Email:</label>
                        <input type="email" name="email" id="email" class="form-control" value="<?= empty($message) ? set_value('email') : '' ?>" placeholder="Nhập email của bạn..." required data-parsley-required-message="Email không được bỏ trống.">
                    </div>
                    <div class="form-group mt-3">
                        <label for="password">Mật khẩu:</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Mật khẩu..." required data-parsley-required-message="Mật khẩu không được bỏ trống." minlength="4" data-parsley-minlength-message="Mật khẩu phải có ít nhất 4 ký tự.">
                    </div>
                    <div class="form-group text-center mt-3">
                        <input type="submit" name="submit" class="btn btn-success" value="Đăng nhập">
                    </div>
                    <div class="form-group text-center mt-3">
                        <p>Chưa có tài khoản? <a href="<?= site_url('dang-ky') ?>">Đăng ký</a> ngay.</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>