<h1 class="text-center"><b>Chào mừng bạn đến với phòng chat</b></h1>
<div class="row justify-content-md-center">
    <div class="col col-md-4 mt-5">

        <?php if (!empty(form_error('submit'))) : ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <?= form_error('submit') ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Đóng">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif ?>

        <?php if (!empty($message)) : ?>
            <div class="alert alert-success">
                <?= $message ?>
            </div>
        <?php endif ?>
        <div class="card">
            <div class="card-header">
                <b>Đăng Ký Tài Khoản Chat</b>
            </div>
            <div class="card-body">
                <form id="auth_form" action="" method="POST">
                    <div class="form-group mt-3">
                        <label for="username">Tên tài khoản:</label>
                        <input type="text" name="username" id="username" class="form-control" value="<?= empty($message) ? set_value('username') : '' ?>" placeholder="Nhập tên tài khoản của bạn..." required data-parsley-required-message="Tên tài khoản không được bỏ trống">
                    </div>
                    <div class="form-group mt-3">
                        <label for="email">Email:</label>
                        <input type="email" name="email" id="email" class="form-control" value="<?= empty($message) ? set_value('email') : '' ?>" placeholder="Nhập email của bạn..." required data-parsley-required-message="Email không được bỏ trống">
                    </div>
                    <div class="form-group mt-3">
                        <label for="password">Mật khẩu:</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Mật khẩu..." required data-parsley-required-message="Mật khẩu không được bỏ trống" minlength="4" data-parsley-minlength-message="Mật khẩu phải có ít nhất 4 ký tự">
                    </div>
                    <div class="form-group text-center mt-3">
                        <input type="submit" name="submit" class="btn btn-success" value="Đăng ký">
                    </div>
                    <div class="form-group text-center mt-3">
                        <p>Đã có tài khoản? <a href="<?= site_url('dang-nhap') ?>">Đăng nhập</a> ngay.</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>