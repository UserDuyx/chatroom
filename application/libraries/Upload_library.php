<?php
class Upload_library
{
    var $CI = '';
    function __construct()
    {
        $this->CI = &get_instance();
    }
    /**
     * Tải lên hình ảnh
     * 
     * @param string $upload_path đường dẫn đến thư mục lưu trữ
     * @param string $file_name tên ảnh
     * @return array $data chứa dữ liệu trả về
     */
    function upload($upload_path = '', $file_name = '')
    {
        //lấy thông tin cấu hình upload
        $config = $this->config($upload_path);
        $this->CI->load->library('upload', $config);

        if ($this->CI->upload->do_upload($file_name)) {
            $data = $this->CI->upload->data();
        } else {
            $data = $this->CI->upload->display_errors();
        }
        return $data;
    }

    /**
     * Tải lên nhiều hình ảnh
     * 
     * @param string $upload_path đường dẫn đến thư mục lưu trữ
     * @param string $file_name tên ảnh
     * @return array $data chứa dữ liệu trả về
     */
    function multiple_file_upload($upload_path = '')
    {
        $config = $this->config($upload_path);
        $this->CI->load->library('upload', $config);

        $image_list = array();

        $file = $_FILES['image_list'];
        $count = count($file['name']);
        for ($i = 0; $i < $count; $i++) {
            $_FILES['userfile']['name']     = $file['name'][$i];
            $_FILES['userfile']['type']     = $file['type'][$i];
            $_FILES['userfile']['tmp_name'] = $file['tmp_name'][$i];
            $_FILES['userfile']['error']    = $file['error'][$i];
            $_FILES['userfile']['size']     = $file['size'][$i];
            if ($this->CI->upload->do_upload()) {
                $data = $this->CI->upload->data();
                $image_list[] = $data['file_name'];
            } else {
                $data = $this->CI->upload->display_errors();
                return $data;
            }
        }
        return $image_list;
    }

    /**
     * cau hinh du lieu upload
     */
    function config($upload_path = '')
    {
        $config = array();
        $config['upload_path'] = $upload_path; // duong dan thu muc muon luu
        $config['allowed_types'] = 'png|jpg|jpeg|gif|image/jpeg'; //chi cho phep tai loai anh nao len
        $config['max_size'] = '100000'; //dung luong toi da duoc tai len, don vi la KB
        $config['max_width'] = '2000'; //chieu rong
        $config['max_height'] = '2000'; //chieu cao

        return $config;
    }

    function upload_image()
    {
        $url = "http://127.0.0.1:8080/image/v1/add";
        if (isset($_FILES['image']['name'])) {
            $image =  $this->curlSendFile(new CURLFile($_FILES['image']['tmp_name']), $url);
        }
        return json_decode($image);
    }

    function multi_upload_image()
    {
        $url = "http://127.0.0.1:8080/image/v1/add";

        if (isset($_FILES['image_list']['name'])) {
            $count = count($_FILES['image_list']['name']);
            for ($i = 0; $i < $count; $i++) {
                $image_list[] = trim($this->curlSendFile(new CURLFile($_FILES['image_list']['tmp_name'][$i]), $url), "\"");
            }
        }
        return $image_list;
    }

    function curlSendFile(CURLFile $file, $url = '')
    {
        if ($file == null || $url == '')
            return false;
        $post_data = array("file" => $file);
        return $this->postCurl($url, $post_data);
    }

    /**
     * CurlPost request
     * @param $url
     * @param $data
     * @return mixed
     * @author Bill
     */
    function postCurl($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
