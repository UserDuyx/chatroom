<?php
class PChat extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('messages_model');
    }
    function index()
    {

        $id = $_SESSION['user'][$this->url]['uid'];
        if (empty($id)) {
            $this->session->set_flashdata('notice', 'Bạn cần phải đăng nhập mới có thể vào phòng chat.');
            redirect(site_url('dang-nhap'));
        }
        if (empty($id))
            redirect(site_url('dang-nhap'));
        $id = ['uid' => $id];
        $userdata = $this->user_model->get_info($id);
        $userdata->token = $_SESSION['user'][$this->url]['token'];
        $this->data['userdata'][$userdata->uid] = $userdata;

        $sql = 'SELECT uid, user_name, user_profile, user_login_status,(SELECT COUNT(*) 
                                                                        FROM chat_private 
                                                                        WHERE pto = "' . $userdata->uid . '" AND pfrom = chat_user.uid AND status = 0) AS count_status
                FROM chat_user';
        $this->data['private_user'] = $this->user_model->manual_query($sql);

        $this->data['title']    = 'Tin nhắn riêng';
        $this->data['temp']     = 'room/private/index';
        $this->load->view('room/layout', $this->data);
    }
    function handle()
    {
        if ($this->input->post('action') && $this->input->post('action') == 'fetch_chat') {
            $sql = "
                SELECT a.user_name AS from_user_name, b.user_name AS to_user_name, pmsg, time, status, pfrom, pto
                FROM chat_private
                JOIN chat_user a ON chat_private.pfrom = a.uid
                JOIN chat_user b ON chat_private.pto = b.uid
                WHERE (chat_private.pfrom = '" . $this->input->post('from') . "' AND chat_private.pto = '" . $this->input->post('to') . "')
                OR (chat_private.pfrom = '" . $this->input->post('to') . "' AND chat_private.pto = '" . $this->input->post('from') . "')
            ";

            $result = $this->messages_model->manual_query($sql);

            $row_class = '';
            $background_class = '';
            $user_name = '';
            $html_data = '';
            $time = '';

            foreach ($result as $item) {
                $time = $this->messages_model->get_time_ago($item->time);

                if ($item->pfrom == $this->input->post('from')) {
                    $row_class = 'row justify-content-end';
                    $background_class = 'alert-success';
                    $user_name = 'Bạn';
                } else {
                    $row_class = 'row justify-content-start';
                    $background_class = 'alert-primary';
                    $user_name = $item->from_user_name;
                }
                $html_data .= '<div class="' . $row_class . '"><div class="col-sm-10"><div class="shadow alert ' . $background_class . '"><b>' . $user_name . ': </b> ' . $item->pmsg . '<div class="text-right"><small><i>' . $time . '.</i></small></div></div></div></div>';
                //reset
                $sql = '';

                //make new sql
                $sql = 'UPDATE chat_private
                    SET status = 1
                    WHERE pfrom = "' . $item->pfrom . '" AND pto = "' . $item->pto . '" AND status = 0
                ';
                $conn = mysqli_connect('127.0.0.1', 'root', '', 'chatroom');
                $conn->query($sql);
            }
            print_r($html_data);
        }
    }
}
