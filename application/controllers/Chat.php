<?php

namespace MyApp;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

date_default_timezone_set('Asia/Ho_Chi_Minh');

class Chat implements MessageComponentInterface
{
    protected $clients;
    protected $uid;
    public $connDB;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->connDB = mysqli_connect('127.0.0.1', 'root', '', 'chatroom');
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        $querystring = $conn->httpRequest->getUri()->getQuery();
        parse_str($querystring, $queryarray);
        if (!empty($queryarray)) {
            $sql = "UPDATE chat_user
                    SET user_connection_id = " . $conn->resourceId . "
                    WHERE user_token ='" . $queryarray['token'] . "'";
            $this->connDB->query($sql);
        } 
        
        echo "New connection! ({$conn->resourceId})\n";
        
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $numRecv = count($this->clients) - 1;
        echo sprintf(
            'Connection %d sending message "%s" to %d other connection%s' . "\n",
            $from->resourceId,
            $msg,
            $numRecv,
            $numRecv == 1 ? '' : 's'
        );

        $data = json_decode($msg, true);
        $time = time();
        $data['dt'] = $this->get_time_ago($time);
        //Load name user from db
        $sql = "SELECT user_name FROM chat_user WHERE uid='" . $data['userId'] . "'";

        $result = $this->connDB->query($sql);
        $username = $result->fetch_assoc();

        if (isset($data['command']) && $data['command'] == 'private') {
            //Reset query
            $sql = '';

            //Insert chat data to db
            $sql = "INSERT INTO chat_private (pfrom, pto, pmsg, time) 
                    VALUES('" . $data['userId'] . "', '" . $data['to'] . "', '" . $data['msg'] . "', " . $time . ")";
            $this->connDB->query($sql);

            //Reset query
            $sql = '';
            $sql = "SELECT user_name, user_connection_id FROM chat_user WHERE uid='" . $data['to'] . "'";
            $result = $this->connDB->query($sql);
            $arr_result = $result->fetch_assoc();
            $sender = $username['user_name'];
            $receiver_connection_id = $arr_result['user_connection_id'];

            foreach ($this->clients as $client) {
                if ($from == $client) {
                    $data['from'] = 'Bạn';
                } else {
                    $data['from'] = $sender;
                }
                if ($client->resourceId == $receiver_connection_id || $from == $client) {
                    $client->send(json_encode($data));
                }
            }
        } else {

            //Reset query
            $sql = '';

            //Insert chat data to db
            $sql = "INSERT INTO chat_messages (user_uid, user_name, msg, time) 
                    VALUES('" . $data['userId'] . "', '" . $username['user_name'] . "', '" . $data['msg'] . "', " . $time . ")";
            $this->connDB->query($sql);

            foreach ($this->clients as $client) {
                // if ($from !== $client) {
                //     // The sender is not the receiver, send to each client connected
                //     $client->send($msg);
                // }
                if ($from == $client) {
                    $data['from'] = 'Bạn';
                } else {
                    $data['from'] = $username['user_name'];
                }
                $client->send(json_encode($data));
            }
        }
    }
    function get_time_ago($time = "")
    {
        $time_difference = time() - $time;

        if ($time_difference < 1) {
            return 'Vài giây trước.';
        }
        $condition = array(
            12 * 30 * 24 * 60 * 60 =>  'năm',
            30 * 24 * 60 * 60       =>  'tháng',
            24 * 60 * 60            =>  'ngày',
            60 * 60                 =>  'giờ',
            60                      =>  'phút',
            1                       =>  'giây'
        );

        foreach ($condition as $secs => $str) {
            $d = $time_difference / $secs;

            if ($d >= 1) {
                $t = round($d);
                return 'Khoảng' . $t . ' ' . $str . ($t > 1 ? 'giây' : '') . ' trước.';
            }
        }
    }
    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
