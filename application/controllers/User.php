<?php


class User extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('messages_model');
        $this->load->library('upload_library');
    }

    function index()
    {

        $id = $_SESSION['user'][$this->url]['uid'];

        $userdata = $this->user_model->get_info(array('uid' => $id));
        if (!$userdata) {
            redirect(site_url());
        }
        $this->data['userdata'][$this->url] = $userdata;
        if ($this->input->post()) {
            if (!$this->name_check()) {
                $this->session->set_flashdata('notice', 'Một cái tên quá khiễm nhã, vui lòng chọn một tên khác.');
                redirect(site_url('ca-nhan/' . $this->data['userdata'][$this->url]->uid));
            }
            $data = array(
                'user_name' => $this->input->post('username')
            );
            if (!empty($this->input->post('password'))) {
                if (strlen($this->input->post('password')) > 3) {
                    $data['user_password'] = md5($this->input->post('password'));
                } else {
                    $this->session->set_flashdata('notice', 'Anh bạn à, đừng chỉnh sửa mã HTML như thế chứ, lỗi đó. Vui lòng nhập đủ mật khẩu 4 ký tự.');
                    redirect(site_url('ca-nhan/' . $this->data['userdata'][$this->url]->uid));
                }
            }

            if (!empty($_FILES['profile']['name'])) {

                $upload_data = $this->upload_library->upload('./public/img', 'profile');
                $data['user_profile'] = $upload_data['file_name'];

                $product_image = './public/img/' . $this->data['userdata'][$this->url]->user_profile;
                if (file_exists($product_image)) {
                    unlink($product_image);
                }
            }
            $this->user_model->update(array('uid' => $id), $data);
            $this->messages_model->update(array('user_uid' => $id), ['user_name' => $this->input->post('username')]);
            redirect(site_url('ca-nhan/' . $this->data['userdata'][$this->url]->uid));
        }
        $this->data["title"] = "Thông tin tài khoản";
        $this->data['temp'] = "room/user/profile";
        $this->load->view('room/layout', $this->data);
    }

    function login()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('submit', 'Submit', 'callback_email_check');
            if ($this->form_validation->run()) {
                $this->form_validation->set_rules('submit', 'Submit', 'callback_password_check');
                if ($this->form_validation->run()) {
                    $input['user_email'] = $this->input->post('email');
                    $userdata = $this->user_model->get_info($input);
                    // $data['user'][$userdata->id] = $userdata->id;
                    // $this->session->set_userdata($data);
                    $_SESSION['user'][$userdata->uid] = [
                        'uid' => $userdata->uid,
                        'token' => $userdata->user_token
                    ];
                    $this->user_model->update(['uid' => $userdata->uid], ['user_login_status' => 1, 'user_token ' => md5($userdata->uid)]);
                    redirect(site_url('trang-chu/' . $userdata->uid));
                }
            }
        }

        $this->data["title"] = "Đăng nhập";
        $this->data['temp'] = "room/user/login";
        $this->load->view('room/layout', $this->data);
    }

    function register()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('submit', 'Submit', 'callback_name_check');
            if ($this->form_validation->run()) {
                $this->form_validation->set_rules('submit', 'Submit', 'callback_email_check');
                if ($this->form_validation->run()) {
                    $this->form_validation->set_rules('submit', 'Submit', 'callback_password_min');
                    if ($this->form_validation->run()) {
                        $data = array(
                            'uid'                    => uniqid(),
                            'user_name'              => $this->input->post('username'),
                            'user_email'             => $this->input->post('email'),
                            'user_password'          => md5($this->input->post('password')),
                            'user_profile'           => $this->__make_avatar(strtoUpper($this->input->post('username'))[0]),
                            'user_verification_code' => md5(uniqid()),
                            'user_status'            => 1,
                            'user_created'           => date('Y-m-d H:i:s'),
                        );

                        $this->user_model->create($data);
                        $this->data['message'] = 'Đăng ký thành công, giờ bạn đã có thể đăng nhập để vào phòng chat.'; //'Đăng ký thành công, một email đã được gửi tới ' . $data['user_email'] . ', hãy vào email của bạn và xác nhận tài khoản!';
                    }
                }
            }
        }

        $this->data["title"] = "Đăng ký";
        $this->data['temp'] = "room/user/register";
        $this->load->view('room/layout', $this->data);
    }

    function logout()
    {
        $this->user_model->update(['uid' => $this->url], ['user_login_status' => 0]);
        $_SESSION['user'][$this->url]['uid'] = [];
        // $this->session->sess_destroy();
        redirect(site_url('dang-nhap'));
    }

    function name_check()
    {
        $name = $this->input->post('username');
        $array = array('cặc', 'lồn', 'chim', 'bướm', 'mẹ', 'mày', 'địt', 'fuck', 'buồi', 'đéo', 'vãi', 'cụ', 'bố', 'đụ', 'lỗ', 'nhị', 'thong', 'dit');

        if (!$this->teststringforbadwords($name, $array)) {
            $this->form_validation->set_message(__FUNCTION__, 'Một cái tên quá khiễm nhã, vui lòng chọn một tên khác.');
            return FALSE;
        }
        return TRUE;
    }


    function teststringforbadwords($string, $banned_words)
    {
        foreach ($banned_words as $banned_word) {
            if (stristr($string, $banned_word)) {
                return false;
            }
        }
        return true;
    }

    function email_check()
    {
        $where = array('user_email' => $this->input->post('email'));

        if ($this->user_model->check_exists($where)) {
            if ($this->uri->rsegment(2) == "register") {
                $this->form_validation->set_message(__FUNCTION__, 'Email đã tồn tại, vui lòng chọn một địa chỉ email khác.');
                return FALSE;
            }
        } else {
            if ($this->uri->rsegment(2) == "login") {
                $this->form_validation->set_message(__FUNCTION__, 'Email không tồn tại, vui lòng kiểm tra lại.');
                return FALSE;
            }
        }
        return TRUE;
    }

    function password_check()
    {
        $input['user_email'] = $this->input->post('email');
        $password = $this->input->post('password');
        $validate = $this->user_model->get_info($input, 'user_password');
        if ($validate->user_password != md5($password)) {
            $this->form_validation->set_message(__FUNCTION__, 'Sai mật khẩu, vui lòng kiểm tra lại.');
            return FALSE;
        }
        return TRUE;
    }

    function password_min()
    {
        if (strlen($this->input->post('password')) < 4) {
            $this->form_validation->set_message(__FUNCTION__, 'Anh bạn à, đừng chỉnh sửa mã HTML như thế chứ, lỗi đó. Vui lòng nhập đủ mật khẩu 4 ký tự.');
            return FALSE;
        }
        return TRUE;
    }

    function __make_avatar($character)
    {
        $img_name = time() . ".png";
        $path = '/public/img/' . $img_name;
        $image = imagecreate(200, 200);
        $r = rand(0, 255);
        $g = rand(0, 255);
        $b = rand(0, 255);
        imagecolorallocate($image, $r, $g, $b);
        $textcolor = imagecolorallocate($image, 255, 255, 255);
        $font = dirname(__DIR__, 2) . '/public/font/arial.ttf';
        imagettftext($image, 100, 0, 50, 150, $textcolor, $font, $character);
        imagepng($image, '.' . $path);
        imagedestroy($image);
        return $img_name;
    }
}
