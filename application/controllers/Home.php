<?php
class Home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('messages_model');
    }
    function index()
    {


        $id = $_SESSION['user'][$this->url]['uid'];
        if (empty($id)) {
            $this->session->set_flashdata('notice', 'Bạn cần phải đăng nhập mới có thể vào phòng chat.');
            redirect(site_url('dang-nhap'));
        } else {
            $id = ['uid' => $id];
            $userdata = $this->user_model->get_info($id);
            $input['order'] = array('id', 'ASC');

            $this->data['userdata'][$userdata->uid] = $userdata;
            $this->data['messages'] = $this->messages_model->get_list($input);

            foreach ($this->data['messages'] as $data) {
                $data->time = $this->user_model->get_time_ago($data->time);
            }

            $this->data['online'] = $this->user_model->get_list();
        }

        $this->data['title'] = 'Trang Chủ';
        $this->data['temp'] = 'room/home/index';
        $this->load->view('room/layout', $this->data);
    }
}
