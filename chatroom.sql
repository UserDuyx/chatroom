-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2021 at 05:37 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatroom`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL,
  `user_uid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `msg` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chat_messages`
--

INSERT INTO `chat_messages` (`id`, `user_uid`, `user_name`, `msg`, `time`) VALUES
(1, '617cd784e93d0', 'Admin', 'Có ai không?', 1635584558),
(2, '617cdb6555174', 'Duyx', 'Không bạn', 1635584574),
(3, '617cd784e93d0', 'Admin', 'Vãi', 1635585174),
(4, '617cdb6555174', 'Duyx', 'Gì thằng oắt con này', 1635585573),
(5, '617cd784e93d0', 'Admin', 'Sủa đi, t ban m bây giờ', 1635585584),
(6, '617cdb6555174', 'Duyx', 'Adudu', 1635586500),
(7, '617cdb6555174', 'Duyx', 'Đừng làm vậy với em anh ơi', 1635586511),
(8, '617cd784e93d0', 'Admin', 'Ngoan', 1635586593),
(9, '617cd784e93d0', 'Admin', 'Đừng\n', 1635586994),
(10, '617cd784e93d0', 'Admin', 'Hello', 1635666787),
(11, '617cd784e93d0', 'Admin', 'Hello\n', 1635667914),
(12, '617cd784e93d0', 'Admin', 'Test', 1635683725),
(13, '617cd784e93d0', 'Admin', 'Lại là mình đây', 1635683776),
(14, '617cd784e93d0', 'Admin', 'Hello', 1635684909),
(15, '617cd784e93d0', 'Admin', 'Please', 1635739820),
(16, '617cd784e93d0', 'Admin', 'Please 2\n', 1635739895),
(17, '617cd784e93d0', 'Admin', 'Test', 1635739921),
(18, '617cdb6555174', 'Duyx', 'test lone test lắm', 1635743747),
(19, '617cdb6555174', 'Duyx', 'sad', 1635770472),
(20, '617cdb6555174', 'Duyx', 'sad t', 1635770561),
(21, '617cdb6555174', 'Duyx', 'ei yo', 1635770624),
(22, '617cdb6555174', 'Duyx', 'buồn', 1635770674),
(23, '617cdb6555174', 'Duyx', 'Mở lại chat', 1635770703),
(24, '617cdb6555174', 'Duyx', 'Mở lại chat 2', 1635770733),
(25, '617cd784e93d0', 'Admin', 'Helo4', 1635775051),
(26, '617cdb6555174', 'Duyx', 'Lô gì bạn', 1635775763);

-- --------------------------------------------------------

--
-- Table structure for table `chat_private`
--

CREATE TABLE `chat_private` (
  `pid` int(11) NOT NULL,
  `pfrom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pto` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pmsg` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chat_private`
--

INSERT INTO `chat_private` (`pid`, `pfrom`, `pto`, `pmsg`, `time`, `status`) VALUES
(1, '617cd784e93d0', '617cdb6555174', 'Hello', 1635683013, 1),
(2, '617cd784e93d0', '617cdb6555174', 'Ei yo', 1635684950, 1),
(3, '617cd784e93d0', '617cdb6555174', 'Trả lời t đi ', 1635739808, 1),
(4, '617f6b90e16e3', '617cd784e93d0', 'Ei test tin nhắn phát', 1635741416, 1),
(5, '617cdb6555174', '617cd784e93d0', 'Hello', 1635742548, 1),
(6, '617cdb6555174', '617cd784e93d0', 'Chào nhóc\n', 1635742664, 1),
(7, '617cd784e93d0', '617cdb6555174', 'Sao thằng cu', 1635742716, 1),
(8, '617cdb6555174', '617cd784e93d0', 'Sao gì mà sao', 1635742932, 1),
(9, '617cd784e93d0', '617cdb6555174', 'Ơ m được', 1635743125, 1),
(10, '617cdb6555174', '617cd784e93d0', 'Sao cơ', 1635743312, 1),
(11, '617cdb6555174', '617cd784e93d0', 'Sao xiếc gì', 1635743392, 1),
(12, '617cdb6555174', '617cd784e93d0', 'Muốn gì nói đi', 1635743414, 1),
(13, '617cd784e93d0', '617cdb6555174', 'Thịt m', 1635743454, 1),
(14, '617cdb6555174', '617cd784e93d0', 'Ei yo', 1635743839, 1),
(15, '617cd784e93d0', '617cdb6555174', 'Sao', 1635743845, 1),
(16, '617cdb6555174', '617cd784e93d0', 'Sao con khỉ', 1635775779, 1),
(17, '617cdb6555174', '617cd784e93d0', ' Bạn không nên sao với chả không sao', 1635775787, 1),
(18, '617cd784e93d0', '617cdb6555174', 'Gì cơ', 1635775878, 1),
(19, '617cdb6555174', '617cd784e93d0', 'Á à, thì ra m chọn cái chết', 1635775945, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chat_user`
--

CREATE TABLE `chat_user` (
  `id` int(11) NOT NULL,
  `uid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_profile` text COLLATE utf8_unicode_ci NOT NULL,
  `user_status` tinyint(4) NOT NULL DEFAULT 0,
  `user_verification_code` text COLLATE utf8_unicode_ci NOT NULL,
  `user_login_status` tinyint(4) NOT NULL DEFAULT 0,
  `user_created` datetime NOT NULL,
  `user_token` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `user_connection_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chat_user`
--

INSERT INTO `chat_user` (`id`, `uid`, `user_name`, `user_email`, `user_password`, `user_profile`, `user_status`, `user_verification_code`, `user_login_status`, `user_created`, `user_token`, `user_connection_id`) VALUES
(3, '617cd784e93d0', 'Admin', 'duyx4541@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '1635571588.png', 1, '0b049a6bf86a52b427668e9af691b4f8', 1, '2021-10-30 12:26:28', '364d11d47fff0e868a96b1e715d66934', 55),
(4, '617cdb6555174', 'Duyx', 'duyx4542@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'logo2.png', 1, '950c97fa549dcb2736118ea39798239a', 0, '2021-10-30 12:43:01', '4660298662e42eb3c23be6967b53acb9', 55),
(5, '617f6b90e16e3', 'Autobot', 'duyx4543@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '1635740560.png', 1, 'd61194f3e07c5656c85c413d63250764', 0, '2021-11-01 11:22:40', 'c3a19c23084074585c10e40ef43dcc5d', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat_messages`
--
ALTER TABLE `chat_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_private`
--
ALTER TABLE `chat_private`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `chat_user`
--
ALTER TABLE `chat_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat_messages`
--
ALTER TABLE `chat_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `chat_private`
--
ALTER TABLE `chat_private`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `chat_user`
--
ALTER TABLE `chat_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
