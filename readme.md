# Chatroom by Bế Lãng Duy (DH51903060) and Đặng Minh Luân (DH51900957)
Đồ án mạng máy tính. Sử dụng PHP, JS, HTML5, CSS, AJAX, websocket (Ratchet).
***
## Tính năng:
- `User`
    - `Login`: đăng nhập tài khoản.
    - `Logout`: đăng kí tài khoản.
    - `Update profile`: chỉnh sửa thông tin các nhân, ảnh đại diện.

- `Chat`
    - `Public chat`: Chat với mọi người trong cùng một phòng.
    - `Private chat`: Chat riêng với một người nào đó.
***

## Cài đặt:

- `Chú ý`: phần mềm dùng phpmyadmin để quản lý database nên cần cài đặt xampp phiên bản PHP 7.4.25 để sử dụng.

- Truy cập vào thư mục PATH_TO_YOUR_XAMPP/htdocs và chạy lệnh: `git clone https://UserDuyx@bitbucket.org/UserDuyx/chatroom.git` để tải về source code.

- `Bước 1`: Cài đặt database.
    - `1.1` Khởi động xampp, bật 2 module Apache và MySQL.
    - `1.2` Truy cập phpmyadmin `127.0.0.1/phpmyadmin`.
    - `1.3` Tạo mới một table có tên "`chatroom`" với mã hóa `utf8_unicode_ci`.
    - `1.4` Nhập file sql trong source code (trong thư mục gốc).

- `Bước 2`: Khởi chạy websocket. 
    - Khởi động terminal chạy các lệnh sau (trước khi chạy phải truy cập vào thư mục htdocs của xampp trước):

    - Nếu chưa truy cập vào thư mục htdocs:
    ```
    $ cd PATH_TO_YOUR_XAMPP/htdocs/chatroom/application/controllers
    $ php server.php
    
    ```
    - Nếu đã truy cập:
    ```
    $ cd chatroom/application/controllers
    $ php server.php
    ```
## Sử dụng:
Sau khi cài đặt xong, mở trình duyệt lên và truy cập vào "`127.0.0.1/chatroom`" và bắt đầu quá trình sử dụng.

[Video demo](https://1drv.ms/v/s!Ao-FdMfRREw2mQfwqx0vfBfWPrpz)